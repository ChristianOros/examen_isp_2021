package examen_isp_2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;


public class FrameTest extends JFrame implements ActionListener {
    public static final String Nume = "FileName.txt";
    private JPanel mainPanel;

    private JTextArea textArea;
    private JButton button;

    public FrameTest(String _title) {

        super(_title);


        initComponents();


        pack();

        setVisible(true);


        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] _args) {
        FrameTest _frame = new FrameTest("Frame Test");
    }

    private void initComponents() {

        setLayout(new FlowLayout());

        mainPanel = new JPanel(new FlowLayout());

        textArea = new JTextArea(10, 10); //10 columns, 10 rows
        button = new JButton("Click me!");


        mainPanel.add(textArea);
        mainPanel.add(button);

        add(mainPanel);

        button.addActionListener(this);
    }


    public void actionPerformed(ActionEvent _actionEvent) {
        try {
            File myObj = new File(Nume);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter myWriter = new FileWriter(Nume);
            myWriter.write(textArea.getText());
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
